class_name Piece
extends CharacterBody3D

enum { 
	MOVE_MODE_TAIL, 
	MOVE_MODE_MANUAL, 
	MOVE_MODE_ONE_POINT 
}

@export_node_path("Piece") var _head
@export var index = 0

@onready var terrain_mgr: TerrainMgr = get_node("/root/Root/TerrainMgr")

var head: Piece = null
var speed: float = 2.0
var prev_pos = Vector3.ZERO

# Head-only data
var apply = false
var children = []

var move_mode = MOVE_MODE_ONE_POINT
var ai_target = Vector3.ZERO


func _ready():
	if _head != null:
		head = get_node_or_null(_head)
	
	if is_head():
		children.push_back(self)
	else:
		head.register_as_tail(self)
		move_mode = MOVE_MODE_TAIL
	
	$Label3D.text = self.name


func _physics_process(delta):
	
	match move_mode:
		MOVE_MODE_TAIL:
			tail_move(delta)
		MOVE_MODE_MANUAL:
			manual_move(delta)
		MOVE_MODE_ONE_POINT:
			one_point_move(delta)
	
	var dx = floor(global_position.x) - floor(prev_pos.x)
	var dz = floor(global_position.z) - floor(prev_pos.z)
	
	if dx != 0 or dz != 0:
		var upos = Vector2(global_position.x, global_position.z)
		terrain_mgr.add_weight(upos, 15 if is_head() else 5)
		prev_pos = global_position
	
	move_and_slide()


func manual_move(delta):
	var move = Input.get_action_strength("move_fwd")
	if abs(move) > 0.01:
		apply = true
		speed = lerp(speed, get_master_speed(), 0.1)
		var direction = (to_global(Vector3(0,0, -move)) - to_global(Vector3(0,0,0))).normalized()
		velocity = direction * speed
		
		var rotate = Input.get_axis("turn_left", "turn_right")
		if abs(rotate) > 0.01:
			rotation.y += -rotate * speed * 0.5 * delta
		
	else:
		apply = false
		velocity = Vector3.ZERO


func one_point_move(delta):
	var direction = ai_target - global_position
	direction.y = 0
	
	var distance = direction.length()
	
	speed = get_master_speed()
	if distance > 0.1:
		velocity = direction.normalized() * speed
		rotation.y = Vector2(velocity.x, velocity.z).angle_to(Vector2.UP)
	else:
		apply = false
		velocity = Vector3.ZERO


func tail_move(delta):
	assert(!is_head())
	
	var target = head.children[index - 1]
	var direction: Vector3 = (target.global_position - global_position)
	var dist = direction.length()
	
	var speed = min(head.speed, get_desired_speed())
	if dist > (speed * 0.001 * 1.1 + 2.5):
		velocity = direction.normalized() * speed
		rotation.y = Vector2(velocity.x, velocity.z).angle_to(Vector2.UP)
	else:
		velocity = Vector3.ZERO



func is_head() -> bool:
	return head == null


func get_desired_speed() -> float:
	var upos = Vector2(global_position.x, global_position.z)
	return 0.5 * (1.0 + terrain_mgr.get_weight(upos) / 10.0) + 2.0


func get_master_speed() -> float:
	assert(is_head())
	
	var t_speed = get_desired_speed()
	
	for child in children:
		var c_speed = child.get_desired_speed()
		if c_speed < t_speed:
			t_speed = c_speed
	
	return t_speed


func register_as_tail(item: Piece):
	print(self.name, " notified of ", item.name, " presence ")
	children.push_back(item)
	
	var t_speed = item.get_desired_speed()
	if t_speed < speed:
		speed = t_speed
