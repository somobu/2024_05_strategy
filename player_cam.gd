extends Node3D

var mouse_move_scale = Vector2(0.1, 0.1)
var max_zoom = 1000

var last_mouse_pos = Vector2.ZERO

func _input(event):
	if event is InputEventMouseMotion:
		last_mouse_pos = event.position
		
		if Input.get_action_strength("grab_map") > 0.01:
			var cpos = $CamBase/Camera.position.z * 0.01
			global_position += Vector3(
				-event.relative.x * mouse_move_scale.x * cpos,
				0,
				-event.relative.y * mouse_move_scale.y * cpos
			)


func _process(delta):
	
	if Input.is_action_just_pressed("zoom_in"): 
		$CamBase/Camera.position.z /= 1.1
	if Input.is_action_just_pressed("zoom_out"): 
		$CamBase/Camera.position.z *= 1.1
	
	if $CamBase/Camera.position.z > max_zoom:
		$CamBase/Camera.position.z = max_zoom
	
	if Input.is_action_just_released("pin_map"):
		var s: Vector3 = $CamBase/Camera.project_ray_origin(last_mouse_pos)
		var n: Vector3 = $CamBase/Camera.project_ray_normal(last_mouse_pos)
		
		var state = get_world_3d().direct_space_state
		if state == null: return
		
		var params = PhysicsRayQueryParameters3D.create(s, s + n * max_zoom * 10)
		var rz = state.intersect_ray(params)
		if rz == null or not "collider" in rz: return
		
		var pin: Node3D = get_node("/root/Root/Pin")
		pin.global_position = rz["position"]
		
		var piece: Piece = get_node("/root/Root/HeadPiece")
		piece.ai_target = rz["position"]
