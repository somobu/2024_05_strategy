class_name Terrain
extends Node3D

const POINTS_PER_SIDE = 100
const POINTS_GAP = 1

const DATA_W = POINTS_PER_SIDE + 2*POINTS_GAP
const DATA_H = POINTS_PER_SIDE + 2*POINTS_GAP

var data = PackedFloat32Array()
var has_non_zero_data = false
var data_changed = false

var decay_timeout = 5.0
var current_decay_timeout = 0

func _ready():
	$MeshInstance3D.mesh = $MeshInstance3D.mesh.duplicate(true)
	
	$BushParticles3D.process_material = $BushParticles3D.process_material.duplicate(true)
	$BushParticles3D.process_material.set_shader_parameter("amount", $BushParticles3D.amount)
	
	$SpruceParticles.process_material = $SpruceParticles.process_material.duplicate(true)
	$SpruceParticles.process_material.set_shader_parameter("amount", $SpruceParticles.amount)
	
	data.resize(DATA_W * DATA_H)
	
	for y in range(DATA_H):
		for x in range(DATA_W):
			set_data(x-POINTS_GAP, y-POINTS_GAP, 0)
	
	current_decay_timeout = (get_instance_id() % int(decay_timeout * 100)) / 100


func get_data(x: int, y: int) -> float:
	return data[DATA_W * (y+POINTS_GAP) + (x+POINTS_GAP)]


func set_data(x: int, y: int, value: float):
	data[DATA_W * (y+POINTS_GAP) + (x+POINTS_GAP)] = value
	data_changed = true
	has_non_zero_data = true

func commit_to_shader():
	$MeshInstance3D.mesh.material.set_shader_parameter("data", data)
	$BushParticles3D.process_material.set_shader_parameter("data", data)
	$SpruceParticles.process_material.set_shader_parameter("data", data)


func _physics_process(delta):
	if has_non_zero_data:
		current_decay_timeout += delta
		
		if current_decay_timeout > decay_timeout:
			current_decay_timeout = 0
			has_non_zero_data = false
			
			for y in range(DATA_H):
				for x in range(DATA_W):
					var value = data[DATA_W * y + x]
					
					if value > 0:
						value *= 0.95
						value = floor(value * 10) / 10
						if value < 0: value = 0.0
						if value > 0: has_non_zero_data = true
						
						set_data(x-POINTS_GAP, y-POINTS_GAP, value)
		
		if data_changed and $VisCheck.is_on_screen():
			commit_to_shader()
			data_changed = false


func add_weight(position: Vector2, weight: int):
	var x = floori(position.x)
	var y = floori(position.y)
	
	add_weight_int(x-1, y-1, weight * 0.25)
	add_weight_int(x-1, y, weight * 0.5)
	add_weight_int(x-1, y+1, weight * 0.25)
	
	add_weight_int(x, y-1, weight * 0.5)
	add_weight_int(x, y, weight)
	add_weight_int(x, y+1, weight * 0.5)
	
	add_weight_int(x+1, y-1, weight * 0.25)
	add_weight_int(x+1, y, weight * 0.5)
	add_weight_int(x+1, y+1, weight * 0.25)


func add_weight_int(x: int, y: int, weight: float):
	if x < -POINTS_GAP or x >= POINTS_PER_SIDE+POINTS_GAP: return
	if y < -POINTS_GAP or y >= POINTS_PER_SIDE+POINTS_GAP: return
	
	var value = get_data(x,y)
	value += weight
	if value > 100:
		value = 100
	
	set_data(x, y, value)


func get_weight(x: int, y: int):
	return data[DATA_W * (y+POINTS_GAP) + (x+POINTS_GAP)]


func get_lerp_weight(position: Vector2):
	var x = floori(position.x)
	var y = floori(position.y)
	if x < 0 or x >= POINTS_PER_SIDE: assert(false, "Out of bounds!")
	if y < 0 or y >= POINTS_PER_SIDE: assert(false, "Out of bounds!")
	
	var dx = position.x - x;
	var dy = position.y - y;
	
	var x1 = get_weight(x, y)
	var x2 = get_weight(x, y+1)
	var dx1 = get_weight(x+1, y)
	var dx2 = get_weight(x+1, y+1)
	
	var r1 = lerp(x1, dx1, dx);
	var r2 = lerp(x2, dx2, dx);
	
	return lerp(r1, r2, dy)


func _on_screen_entered():
	commit_to_shader()


func _on_screen_exited():
	pass # Replace with function body.
