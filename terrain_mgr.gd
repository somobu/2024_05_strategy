class_name TerrainMgr
extends Node3D

@onready var terrain_ref = preload("res://terrain.tscn")


func _ready():
	for x in range(-5, 5, 1):
		for y in range(-5, 5, 1):
			var instance = terrain_ref.instantiate()
			instance.name = "T_%d_%d" % [x, y]
			instance.position = Vector3(x,0,y) * Terrain.POINTS_PER_SIDE
			add_child(instance)


func get_weight(pos: Vector2) -> float:
	
	# Some weird shitfuckery happens around zero
	var npos = pos
	if npos.x < 0.0001: npos.x = 0.0001
	if npos.y < 0.0001: npos.y = 0.0001
	
	var pos_glob = Vector2i(
		floor(npos.x / Terrain.POINTS_PER_SIDE), 
		floor(npos.y / Terrain.POINTS_PER_SIDE)
	)
	
	var pos_loc = Vector2(
		npos.x - pos_glob.x * Terrain.POINTS_PER_SIDE,
		npos.y - pos_glob.y * Terrain.POINTS_PER_SIDE
	)
	
	if pos_loc.x < 0: pos_loc.x += Terrain.POINTS_PER_SIDE
	if pos_loc.y < 0: pos_loc.y += Terrain.POINTS_PER_SIDE
	
	var node: Terrain = get_node_or_null("T_%d_%d" % [pos_glob.x, pos_glob.y])
	if node != null:
		return node.get_lerp_weight(pos_loc)
		
	return 0


func add_weight(pos: Vector2, weight: int):
	var pos_glob = Vector2i(
		floor(pos.x / Terrain.POINTS_PER_SIDE), 
		floor(pos.y / Terrain.POINTS_PER_SIDE)
	)
	
	var pos_loc = Vector2(
		pos.x - pos_glob.x * Terrain.POINTS_PER_SIDE,
		pos.y - pos_glob.y * Terrain.POINTS_PER_SIDE
	)
	
	if pos_loc.x < 0: pos_loc.x += Terrain.POINTS_PER_SIDE
	if pos_loc.y < 0: pos_loc.y += Terrain.POINTS_PER_SIDE
	
	var tp = Terrain.POINTS_PER_SIDE
	add_weight_to_node(pos_glob + Vector2i(-1, -1), pos_loc + Vector2(tp, tp),   weight)
	add_weight_to_node(pos_glob + Vector2i(-1, 0),  pos_loc + Vector2(tp, 0),    weight)
	add_weight_to_node(pos_glob + Vector2i(-1, 1),  pos_loc + Vector2(tp, -tp),  weight)
	
	add_weight_to_node(pos_glob + Vector2i(0, -1),  pos_loc + Vector2(0, tp),    weight)
	add_weight_to_node(pos_glob + Vector2i(0, 0),   pos_loc + Vector2(0, 0),     weight)
	add_weight_to_node(pos_glob + Vector2i(0, 1),   pos_loc + Vector2(0, -tp),   weight)
	
	add_weight_to_node(pos_glob + Vector2i(1, -1),  pos_loc + Vector2(-tp, tp),  weight)
	add_weight_to_node(pos_glob + Vector2i(1, 0),   pos_loc + Vector2(-tp, 0),   weight)
	add_weight_to_node(pos_glob + Vector2i(1, 1),   pos_loc + Vector2(-tp, -tp), weight)



func add_weight_to_node(pos_glob: Vector2i, pos_loc: Vector2, weight: float):
	var node: Terrain = get_node_or_null("T_%d_%d" % [pos_glob.x, pos_glob.y])
	if node != null:
		node.add_weight(pos_loc, weight)

